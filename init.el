;;;;
;; Packages
;;;;

;; Define package repositories
(require 'cl)
(require 'package)

(package-initialize)

(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)


;; Load and activate emacs packages. Do this first so that the
;; packages are loaded before you start trying to modify them.
;; This also sets the load path.
(package-initialize)

;; Download the ELPA archive description if needed.
;; This informs Emacs about the latest versions of all packages, and
;; makes them available for download.
(when (not package-archive-contents)
  (package-refresh-contents))

;; The packages you want installed. You can also install these
;; manually with M-x package-install
;; Add in your own as you wish:
(defvar my-packages
  '(
    ;;font-lock+
    ac-cider
    all-the-icons
    all-the-icons-dired
    async
    auto-complete
    auto-highlight-symbol
    dash
    diff-hl
    cider                            ; integration with a Clojure REPL
    clojure-mode
    clojure-mode-extra-font-locking  ; extra syntax highlighting for clojure

    ido-ubiquitous
    magit                            ; git integration
    neotree
    paredit
    projectile                       ; project navigation
    rainbow-delimiters               ; colorful parens
    smex                             ; Enhances M-x to allow easier execution of commands. Provides
    tagedit                          ; edit html tags like sexps
    with-editor
    ))

;; On OS X, an Emacs instance started from the graphical user
;; interface will have a different environment than a shell in a
;; terminal window, because OS X does not run a shell during the
;; login. Obviously this will lead to unexpected results when
;; calling external utilities like make from Emacs.
;; This library works around this problem by copying important
;; environment variables from the user's shell.
;; https://github.com/purcell/exec-path-from-shell
;;(if (eq system-type 'darwin)
;;    (add-to-list 'my-packages 'exec-path-from-shell))

(ignore-errors ;; This package is only relevant for Mac OS X.
  (when (memq window-system '(mac ns))
    (push 'exec-path-from-shell my-packages)
    (push 'reveal-in-osx-finder my-packages))
  (let ((packages (remove-if 'package-installed-p my-packages)))
    (when packages
      ;; Install uninstalled packages
      (package-refresh-contents)
      (mapc 'package-install packages))))

;;(dolist (p my-packages)
;;  (when (not (package-installed-p p))
;;    (package-install p)))


;; Place downloaded elisp files in ~/.emacs.d/vendor. You'll then be able
;; to load them.
;;
;; For example, if you download yaml-mode.el to ~/.emacs.d/vendor,
;; then you can add the following code to this file:
;;
;; (require 'yaml-mode)
;; (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
;; 
;; Adding this code will make Emacs enter yaml mode whenever you open
;; a .yml file
(add-to-list 'load-path "~/.emacs.d/vendor")


;;;;
;; Customization
;;;;

;; Add a directory to our load path so that when you `load` things
;; below, Emacs knows where to look for the corresponding file.
(add-to-list 'load-path "~/.emacs.d/customizations")

;; Sets up exec-path-from-shell so that Emacs will use the correct
;; environment variables
(load "shell-integration.el")

;; These customizations make it easier for you to navigate files,
;; switch buffers, and choose options from the minibuffer.
(load "navigation.el")

;; These customizations change the way emacs looks and disable/enable
;; some user interface elements
(load "ui.el")

;; These customizations make editing a bit nicer.
(load "editing.el")

;; Hard-to-categorize customizations
(load "misc.el")

;; For editing lisps
(load "elisp-editing.el")

;; Langauage-specific
(load "setup-clojure.el")
(load "setup-js.el")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(coffee-tab-width 2)
 '(inf-clojure-tools-deps-cmd "clojure")
 '(package-selected-packages
   (quote
    (yaml-mode flycheck-clj-kondo cider all-the-icons-dired all-the-icons neotree groovy-mode diff-hl dash auto-complete rainbow-delimiters projectile paredit clojure-mode-extra-font-locking clojure-mode ac-cider web-mode tagedit spinner smex magit js2-mode ido-ubiquitous git-rebase-mode git-commit-mode exec-path-from-shell edts))))

(global-auto-revert-mode t)
(global-auto-complete-mode t)

(require 'clojure-mode)

(define-clojure-indent
  (doto 0)
  (cond-> 0))

(defun figwheel-repl ()
  (interactive)
  (run-clojure "lein trampoline run -m clojure.main figwheel.clj"))

(setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")

;cider autocomplete
(require 'ac-cider)
(add-hook 'cider-mode-hook 'ac-flyspell-workaround)
(add-hook 'cider-mode-hook 'ac-cider-setup)
(add-hook 'cider-repl-mode-hook 'ac-cider-setup)
(eval-after-load "auto-complete"
  '(progn
     (add-to-list 'ac-modes 'cider-mode)
     (add-to-list 'ac-modes 'cider-repl-mode)))

(setq cider-known-endpoints
  '(("employer-api-develop" "qa-app-employer-api-develop" "7070")))

(global-diff-hl-mode t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(add-hook 'prog-mode-hook 'diff-hl-flydiff-mode)
(add-hook 'vc-dir-mode-hook 'diff-hl-flydiff-mode)

(setq projectile-switch-project-action 'neotree-projectile-action)
(setq neo-window-width 30)

;;get $PATH from shell when launched as a normal Mac GUI 
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

(setq css-indent-offset 2)
(setq sass-indent-offset 2)
(setq scss-indent-offset 2)
(put 'erase-buffer 'disabled nil)

;;clj-kondo
(require 'flycheck-clj-kondo)
